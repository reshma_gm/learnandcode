import socket
import psutil
import sys

HOST = '127.0.0.1'
PORT = 9236


def connect_socket_obj():
    try:
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind((HOST, PORT))
    except socket.gaierror as error:
        print("Address-related error connecting to server! %s", error)
    else:
        return server_socket


def connect_with_client(server_socket_obj):
    try:
        conn, addr = server_socket_obj.accept()
    except socket.gaierror as error:
        print("Address-related error connecting with the client! %s", error)
    else:
        print("connection established!")
        return conn, addr


def get_memory_statistics(client_connection):
    process_name = ""                  # apply try catch here
    for process in psutil.process_iter():
        process_name = process_name + process.name() + '\n'
        process_name = process_name + str(psutil.virtual_memory())
        client_connection.send(bytes(process_name, "utf-8"))


def process_client_request(conn):
    try:
        while True:
            received_message = conn.recv(65535)
            decoded_message = received_message.decode("utf-8")
            if decoded_message == "getMemoryStat" and decoded_message != "exit":
                print("Client: ", decoded_message)
                get_memory_statistics(conn)
            elif decoded_message != "getMemoryStat" and decoded_message != "exit":
                print("Client: ", decoded_message)
                send_response = input("Server: ")
                conn.send(bytes(send_response, "utf-8"))
            elif decoded_message == "exit":
                conn.close()
                print("Client Connection closed!")
                sys.exit("done!")
    except socket.error as error:
        print("Error occurred while receiving request from the client! %s", error)


def main():
    try:
        server_socket = connect_socket_obj()
        server_socket.listen()
        connection, address = connect_with_client(server_socket)
        process_client_request(connection)
    except socket.gaierror as error:
        print("Address-related error connecting to server %s", error)
    except socket.error as error:
        print("Error creating a socket!!", error)


if __name__ == '__main__':
    main()