import socket
import subprocess

HOST = '127.0.0.1'
PORT = 9938

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen()
connection, address = server_socket.accept()
print("Connection Established!")
while True:
    recieved_message = connection.recv(65535)
    decoded_recieved_message=recieved_message.decode("utf-8")
    if decoded_recieved_message == "getProcessesStat" and decoded_recieved_message != "exit":
        print("Client: ", decoded_recieved_message)
        cmd = 'ps'
        args = '-aux'
        temp_output = subprocess.Popen([cmd, args], stdout=subprocess.PIPE)
        output = temp_output.communicate()
        connection.send(output[0])
    elif decoded_recieved_message != "getProcessesStat" and decoded_recieved_message != "exit":
        print("Client: ", decoded_recieved_message)
        send_message = input("Server: ")
        connection.send(bytes(send_message, "utf-8"))
    elif decoded_recieved_message == "exit":
        connection.close()
        break

