import socket

HOST = '127.0.0.1'
PORT = 9938

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((HOST, PORT))
while True:
    message_sent = input("Client: ")
    client_socket.send(bytes(message_sent,"utf-8"))
    if message_sent == "getProcessesStat":
        print("Server: Running Process.....!!!")
        received_message = (client_socket.recv(65535)).decode("utf-8")
        print(received_message)
    if message_sent != "getProcessesStat" and message_sent != "exit":
        received_message = (client_socket.recv(1024)).decode("utf-8")
        print("Server: ",received_message)
    if message_sent == "exit":
        break
