import socket


HOST = '127.0.0.1'
PORT = 9236


def main():
    try:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((HOST, PORT))
        while True:
            message_sent = input("Client: ")
            client_socket.send(bytes(message_sent, "utf-8"))
            if message_sent == "exit":
                break
            received_message = (client_socket.recv(65535)).decode("utf-8")
            print("Server: ", received_message)
    except socket.error as error:
        print("Error creating a socket!!", error)

if __name__ == '__main__':
    main()