import socket

HOST = '127.0.0.1'
PORT = 9938

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
    server_socket.bind((HOST, PORT))
    server_socket.listen()
    connection, address = server_socket.accept()
    with connection:
        print('Client Connected!!')
        while True:
            recieved_message = connection.recv(1024)
            decoded_recieved_message=recieved_message.decode("utf-8")
            if decoded_recieved_message == "exit":
                connection.close()
                break
            print("Client: ", decoded_recieved_message)
            send_message = input("Server: ")
            connection.send(bytes(send_message,"utf-8"))