# The below program is to Roll the Dice

import random


def generate_number(max_limit):
    number_on_dice = random.randint(1, max_limit)
    return number_on_dice


def main():
    MAX_NUMBER_ON_DICE = 6
    start_rolling = True
    while start_rolling:
        user_input = input("Ready to roll? Enter Q to Quit")
        if user_input.lower() != "q":
            number_displayed = generate_number(MAX_NUMBER_ON_DICE)
            print("You have rolled a", number_displayed)
        else:
            start_rolling = False


main()
