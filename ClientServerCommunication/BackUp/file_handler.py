import pickle


class FileHandler:
    def __init__(self):
        self.backup_file = "BackUp/backup.pickle"

    def write_serialized_data(self, data):
        with open(self.backup_file, mode='wb') as handler:
            pickle.dump(data, handler, protocol=pickle.HIGHEST_PROTOCOL)

    def read_deserialized_data(self):
        try:
            with open(self.backup_file, 'rb') as handler:
                data = pickle.load(handler)
                return data
        except EOFError:
            data = dict()
            return data
