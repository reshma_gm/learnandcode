import sys
import threading
import time

from ClientServerCommunication.Connection.communication_module import CommunicationModule


class ProcessCommands:
    def __init__(self, requested_data, data_records):
        self.requested_data = requested_data
        self.data_records = data_records
        connection_handler = CommunicationModule()
        self.req_resp_config = connection_handler.get_request_response()

    def update_records(self, key, value, ttl_time):
        try:
            print(self.data_records)
            print(self.requested_data)
            self.data_records[key] = value
            timer = threading.Thread(target=self.ttl_timer, args=[int(ttl_time), key])
            timer.start()
        except Exception as error:
            print("Error while updating the data {}".format(error))

    def ttl_timer(self, ttl, key):
        start_time = 0
        while start_time < ttl:
            time.sleep(1)
            start_time += 1
        del self.data_records[key]

    def process_set_command(self, key, value, ttl):
        if key in self.data_records:
            response = self.req_resp_config['key_exists']
        else:
            self.update_records(key, value, ttl)
            print(self.req_resp_config['set_command'], " : ", self.req_resp_config['done'])
            response = self.req_resp_config['ok_response']
        return response

    def process_get_command(self, key):
        if self.data_records:
            if key in self.data_records:
                response = self.data_records[key]
                print(self.req_resp_config['get_command'], " : ", self.req_resp_config['done'])
            else:
                response = self.req_resp_config['key_not_found']
        else:
            response = self.req_resp_config['key_not_found']
        return response

    def process_put_command(self, key , value):
        if self.data_records:
            if key in self.data_records:
                self.data_records[key] = value
                print(self.req_resp_config['put_command'], " : ", self.req_resp_config['done'])
                response = self.req_resp_config['ok_response']
            else:
                response = self.req_resp_config['key_not_found']
        else:
            response = self.req_resp_config['key_not_found']
        return response

    def execute(self):
        command = self.requested_data[0]
        key = self.requested_data[1]
        if command == 'SET':
            value = self.requested_data[2]
            ttl_time = self.requested_data[3]
            response = self.process_set_command(key, value, ttl_time)
        elif command == 'GET':
            response = self.process_get_command(key)
        elif command == 'PUT':
            value = self.requested_data[2]
            response = self.process_put_command(key, value)
        elif command == 'exit':
            response = self.req_resp_config['exit_command']
            # self.conn.close()  # pass connection close request
            # sys.exit(self.req_resp_config['done'])
        else:
            response = self.req_resp_config['invalid_command']
        return response, self.data_records
