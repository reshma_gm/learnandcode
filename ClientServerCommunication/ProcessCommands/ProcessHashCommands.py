from .ProcessCommands import ProcessCommands


class ProcessHashCommands(ProcessCommands):

    def __init__(self, requested_data, data_records):
        super().__init__(requested_data, data_records)

    def update_hash(self, hash_key, key, value):
        try:
            temp = dict()
            temp[key] = value
            if hash_key in self.data_records:
                self.data_records[hash_key].update(temp)
            else:
                self.data_records[hash_key] = temp
        except Exception as error:
            print("Error while updating the data ".format(error))

    def process_hset_command(self, hash_key, key, value):
        if hash_key in self.data_records:
            if key in self.data_records[hash_key]:
                response = self.req_resp_config['key_exists']
            else:
                self.update_hash(hash_key, key, value)
                print(self.req_resp_config['hset_command'], " : ", self.req_resp_config['done'])
                response = self.req_resp_config['ok_response']
        else:
            self.update_hash(hash_key, key, value)
            print(self.req_resp_config['hset_command'], " : ", self.req_resp_config['done'])
            response = self.req_resp_config['ok_response']
        return response

    def process_hget_command(self, hash_key, key):
        if self.data_records:
            if hash_key in self.data_records:
                if key in self.data_records[hash_key]:
                    response = self.data_records[hash_key][key]
                    print(self.req_resp_config['hget_command'], " : ", self.req_resp_config['done'])
                else:
                    response = self.req_resp_config['key_not_found']
            else:
                response = self.req_resp_config['key_not_found']
            return response

    def execute(self):
        command = self.requested_data[0]
        hash_key = self.requested_data[1]
        key = self.requested_data[2]
        if command == 'HSET':
            value = self.requested_data[3]
            response = self.process_hset_command(hash_key, key, value)
        elif command == 'HGET':
            response = self.process_hget_command(hash_key, key)
        else:
            response = self.req_resp_config['invalid_command']
        return response, self.data_records


