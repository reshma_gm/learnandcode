from .ProcessCommands import ProcessCommands


class ProcessListCommands(ProcessCommands):
    def __init__(self, requested_data, data_records):
        super().__init__(requested_data, data_records)

    def update_list(self, key, value):
        try:
            self.data_records[key] = [value]
        except Exception as error:
            print("Error while updating the data ".format(error))

    def remove_from_list(self, key):
        try:
            if key in self.data_records:
                removed_element = self.data_records[key].pop(0)
                response = removed_element
            else:
                response = self.req_resp_config['null_list']
        except IndexError as error:
            print(self.req_resp_config['lpop_command'], " : ", error)
            response = self.req_resp_config['empty_list']
        return response

    def display_records(self, key, start_index, stop_index):
        if start_index < 0:
            start_index = 0
        if stop_index > len(self.data_records[key]):
            stop_index = len(self.data_records[key])
        if stop_index == -1:
            resulted_list = slice(start_index, len(self.data_records[key]))
        else:
            resulted_list = slice(start_index,stop_index + 1)
        return resulted_list

    def process_lpush_command(self, key, value):
        if key in self.data_records: #LPUSH key value
            if isinstance(self.data_records[key], list):
                self.data_records[key] = [value] + self.data_records[key]
                print(self.req_resp_config['lpush_command'], " : ", self.req_resp_config['done'])
                response = self.req_resp_config['ok_response']
            else:
                response = self.req_resp_config['error']
        else:
            self.update_list(key, value)
            print(self.req_resp_config['lpush_command'], " : ", self.req_resp_config['done'])
            response = self.req_resp_config['ok_response']
        return response

    def process_lpop_command(self, key):
        response = self.remove_from_list(key)
        print(self.req_resp_config['lpop_command'], " : ", self.req_resp_config['done'])
        return response

    def process_lrange_command(self, key, start, stop):
        response = self.display_records(key, int(start), int(stop))
        print(self.req_resp_config['lrange_command'], " : ", self.req_resp_config['done'])
        return response

    def process_llen_command(self, key):
        if key in self.data_records:
            if isinstance(self.data_records[key], list):
                response = str(len(self.data_records[key]))
                print(self.req_resp_config['llen_command'], " : ", self.req_resp_config['done'])
            else:
                response = self.req_resp_config['error']
        else:
            response = str(0)
            print(self.req_resp_config['llen_command'], " : ", self.req_resp_config['done'])
        return response

    def execute(self):
        command = self.requested_data[0]
        key = self.requested_data[1]
        if command == 'LPUSH':
            value = self.requested_data[2]
            response = self.process_lpush_command(key, value)
        elif command == 'LPOP':
            response = self.process_lpop_command(key)
        elif command == 'LRANGE':
            start = self.requested_data[2]
            stop = self.requested_data[3]
            response = self.process_lrange_command(key, start, stop)
        elif command == 'LLEN':
            response = self.process_llen_command(key)
        else:
            response = self.req_resp_config['invalid_command']
        return response, self.data_records
