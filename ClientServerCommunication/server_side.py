import json
import socket

from ClientServerCommunication.CommandsInvoker import CommandsInvoker
from ClientServerCommunication.client_side import *
from ClientServerCommunication.Connection.communication_module import CommunicationModule
from ClientServerCommunication.BackUp.file_handler import FileHandler


class Server():
    def __init__(self, connection):
        self.req_resp_config = connection_handler.get_request_response()
        self.data_records = dict()
        self.hash_records = dict()
        self.list_records = dict()
        self.conn = connection

    def send_response(self, response):
        try:
            self.conn.send(bytes(response, self.req_resp_config['decode_format']))
        except socket.error as error:
            print(error)

    def process_client_request(self):
        try:
            backupfile_handler = FileHandler()
            while True:
                received_request = self.conn.recv(self.req_resp_config['max_bytes_limit'])
                decoded_request = received_request.decode(self.req_resp_config['decode_format'])
                requested_data = json.loads(decoded_request)
                self.data_records = backupfile_handler.read_deserialized_data()
                invoker = CommandsInvoker(requested_data, self.data_records)
                command_handler = invoker.invoke()
                response, self.data_records = command_handler.execute()
                backupfile_handler.write_serialized_data(self.data_records)
                self.send_response(response)
        except socket.error as error:
            print("Error occurred while receiving request from the client! %s", error)


if __name__ == '__main__':
    try:
        connection_handler = CommunicationModule()
        server_socket = connection_handler.create_server_socket()
        server_socket.listen()
        connection, address = connection_handler.connect_with_client(server_socket)
        server = Server(connection)
        server.process_client_request()
    except socket.gaierror as error:
        print("Address-related error connecting to server %s", error)
    except socket.error as error:
        print("Error creating a socket!!", error)