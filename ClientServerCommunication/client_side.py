import sys
import json
import socket
from json.decoder import JSONDecodeError
from ClientServerCommunication.Connection.communication_module import CommunicationModule


class Client:
    def __init__(self, socket):
        self.req_resp_command = connection_handler.get_request_response()
        self.socket = socket

    def send_request(self, requesting_data):
        try:
            request = json.dumps(requesting_data)
            self.socket.sendall(request.encode())
        except JSONDecodeError:
            print("Error at sending request to server:(")

    def receive_response(self):
        try:
            max_byte_limit = self.req_resp_command['max_bytes_limit']
            received_response = (self.socket.recv(max_byte_limit)).decode("utf-8")
            if received_response == self.req_resp_command['exit_command']:
                sys.exit("Connection exited!")
            else:
                print(received_response)
        except socket.error as error:
            print("Error while receiving response from server {}".format(error))

    def send_command(self):
        try:
            input_data = list(input().split(" "))
            self.send_request(input_data)
        except Exception as e:
            print("Error in process_client_request(){}".format(e))
        else:
            return


if __name__ == '__main__':
    try:
        connection_handler = CommunicationModule()
        client_socket = connection_handler.create_client_socket()
        client = Client(client_socket)
        while True:
            client.send_command()
            client.receive_response()
    except socket.error as error:
        print("Error creating a socket!!", error)
