from ClientServerCommunication.ProcessCommands.ProcessCommands import ProcessCommands
from ClientServerCommunication.ProcessCommands.ProcessHashCommands import ProcessHashCommands
from ClientServerCommunication.ProcessCommands.ProcessListCommands import ProcessListCommands


class CommandsInvoker:
    def __init__(self, request, data_records):
        self.command = request[0]
        self.request = request
        self.data_records = data_records

    def invoke(self):
        commands = ['SET', 'GET', 'PUT']
        hash_commands = ['HSET', 'HGET']
        list_commands = ['LPUSH', 'LPOP', 'LLEN', 'LRANGE']

        if self.command in commands:
            return ProcessCommands(self.request, self.data_records)
        elif self.command in hash_commands:
            return ProcessHashCommands(self.request, self.data_records)
        elif self.command in list_commands:
            return ProcessListCommands(self.request, self.data_records)
