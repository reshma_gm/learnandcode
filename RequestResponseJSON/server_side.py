import sys
from communication_module import *
import time
import threading
from file_handler import *

class Server():
    def __init__(self, connection):
        self.req_resp_config = get_request_response()
        self.data_records = dict()
        self.hash_records = dict()
        self.list_records = dict()
        self.conn = connection

    def update_records(self, data):
        try:
            key, value, ttl = data[1], data[2], data[3]
            self.data_records[key] = value
            timer = threading.Thread(target=self.ttl_timer, args=[int(ttl), key])
            timer.start()
        except Exception as error:
            print("Error while updating the data ".format(error))

    def update_hash(self, data):
        try:
            key, field, value = data[1], data[2], data[3]
            temp = dict()
            temp[field] = value
            if key in self.hash_records:
                self.hash_records[key].update(temp)
            else:
                self.hash_records[key] = temp
        except Exception as error:
            print("Error while updating the data ".format(error))

    def update_list(self, data):
        try:
            key, value = data[1], data[2]
            self.list_records[key] = [value]
        except Exception as error:
            print("Error while updating the data ".format(error))

    def remove_from_list(self, key):
        try:
            if key in self.list_records:
                removed_element = self.list_records[key].pop(0)
                return self.send_response(removed_element)
            else:
                return self.send_response(self.req_resp_config['null_list'])
        except IndexError as error:
            print(self.req_resp_config['lpop_command'], " : ", error)
            self.send_response(self.req_resp_config['empty_list'])

    def send_response(self, response):
        try:
            self.conn.send(bytes(response, self.req_resp_config['decode_format']))
        except socket.error as error:
            print(error)

    def ttl_timer(self, ttl, key):
        start_time = 0
        while start_time < ttl:
            time.sleep(1)
            start_time += 1
        del self.data_records[key]

    def process_set_command(self, data):
        if data[1] in self.data_records:
            self.send_response(self.req_resp_config['key_exists'])
        else:
            self.update_records(data)
            print(self.req_resp_config['set_command'], " : ", self.req_resp_config['done'])
            self.send_response(self.req_resp_config['ok_response'])

    def process_get_command(self, data):
        if self.data_records:
            if data[1] in self.data_records:
                self.send_response(self.data_records[data[1]])
                print(self.req_resp_config['get_command'], " : ", self.req_resp_config['done'])
            else:
                self.send_response(self.req_resp_config['key_not_found'])
        else:
            self.send_response(self.req_resp_config['key_not_found'])

    def process_put_command(self, data):
        if self.data_records:
            if data[1] in self.data_records:
                self.data_records[data[1]] = data[2]
                print(self.req_resp_config['put_command'], " : ", self.req_resp_config['done'])
                self.send_response(self.req_resp_config['ok_response'])
            else:
                self.send_response(self.req_resp_config['key_not_found'])
        else:
            self.send_response(self.req_resp_config['key_not_found'])

    def process_hset_command(self, data):
        if data[1] in self.hash_records:
            if data[2] in self.hash_records[data[1]]:
                self.send_response(self.req_resp_config['key_exists'])
            else:
                self.update_hash(data)
                print(self.req_resp_config['hset_command'], " : ", self.req_resp_config['done'])
                self.send_response(self.req_resp_config['ok_response'])
        else:
            self.update_hash(data)
            print(self.req_resp_config['hset_command'], " : ", self.req_resp_config['done'])
            self.send_response(self.req_resp_config['ok_response'])

    def process_hget_command(self, data):
        if self.hash_records:
            if data[1] in self.hash_records:
                if data[2] in self.hash_records[data[1]]:
                    self.send_response(self.hash_records[data[1]][data[2]])
                    print(self.req_resp_config['hget_command'], " : ", self.req_resp_config['done'])
                elif data[2] not in data:
                    self.send_response(self.req_resp_config['key_not_found'])
                else:
                    self.send_response(self.req_resp_config['key_not_found'])
            else:
                self.send_response(self.req_resp_config['key_not_found'])
        else:
            self.send_response(self.req_resp_config['key_not_found'])

    def process_lpush_command(self, data):
        if data[1] in self.list_records:
            if isinstance(self.list_records[data[1]], list):
                self.list_records[data[1]] = [data[2]] + self.list_records[data[1]]
                print(self.req_resp_config['lpush_command'], " : ", self.req_resp_config['done'])
                self.send_response(self.req_resp_config['ok_response'])
            else:
                self.send_response(self.req_resp_config['error'])
        else:
            self.update_list(data)
            print(self.req_resp_config['lpush_command'], " : ", self.req_resp_config['done'])
            self.send_response(self.req_resp_config['ok_response'])

    def process_lpop_command(self, data):
        key = data[1]
        self.remove_from_list(key)
        print(self.req_resp_config['lpop_command'], " : ", self.req_resp_config['done'])

    def process_lrange_command(self, data):
        pass

    def process_llen_command(self, data):
        if data[1] in self.list_records:
            if isinstance(self.list_records[data[1]], list):
                self.send_response(str(len(self.list_records[data[1]])))
                print(self.req_resp_config['llen_command'], " : ", self.req_resp_config['done'])
            else:
                self.send_response(self.req_resp_config['error'])
        else:
            self.send_response(str(0))
            print(self.req_resp_config['llen_command'], " : ", self.req_resp_config['done'])


    def process_command(self, data):
        if data[0] == self.req_resp_config['set_command']:
            self.process_set_command(data)
        elif data[0] == self.req_resp_config['get_command']:
            self.process_get_command(data)
        elif data[0] == self.req_resp_config['put_command']:
            self.process_put_command(data)
        elif data[0] == self.req_resp_config['hset_command']:
            self.process_hset_command(data)
        elif data[0] == self.req_resp_config['hget_command']:
            self.process_hget_command(data)
        elif data[0] == self.req_resp_config['lpush_command']:
            self.process_lpush_command(data)
        elif data[0] == self.req_resp_config['lpop_command']:
            self.process_lpop_command(data)
        elif data[0] == self.req_resp_config['lrange_command']:
            self.process_lrange_command(data)
        elif data[0] == self.req_resp_config['llen_command']:
            self.process_llen_command(data)
        elif data[0] == self.req_resp_config['exit_command']:
            self.send_response(self.req_resp_config['exit_command'])
            self.conn.close()
            sys.exit(self.req_resp_config['done'])
        else:
            self.send_response(self.req_resp_config['invalid_command'])

    def process_client_request(self):
        try:
            while True:
                received_request = self.conn.recv(self.req_resp_config['max_bytes_limit'])
                decoded_request = received_request.decode(self.req_resp_config['decode_format'])
                requested_data = json.loads(decoded_request)
                self.list_records = read_deserialized_data()
                self.process_command(requested_data)
                write_serialized_data(self.list_records)
        except socket.error as error:
            print("Error occurred while receiving request from the client! %s", error)


if __name__ == '__main__':
    try:
        server_socket = create_server_socket()
        server_socket.listen()
        connection, address = connect_with_client(server_socket)
        server = Server(connection)
        server.process_client_request()
    except socket.gaierror as error:
        print("Address-related error connecting to server %s", error)
    except socket.error as error:
        print("Error creating a socket!!", error)