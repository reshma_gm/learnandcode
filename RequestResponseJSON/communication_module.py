import socket
import json


def get_config():
    try:
        with open('config.json', 'r') as f:
            config = json.loads(f.read())
    except Exception as error:
        print("Error: while fetching the server configuration", error)
    else:
        return config


def get_request_response():
    try:
        with open('request_response_config.json', 'r') as f:
            req_res_config = json.loads(f.read())
    except Exception as error:
        print("Error: while fetching the requset and response config", error)
    else:
        return req_res_config


def create_server_socket():
    try:
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_config = get_config()
        server_socket.bind((server_config['host'], server_config['port']))
    except socket.gaierror as error:
        print("Address-related error connecting to server! %s", error)
    else:
        return server_socket


def create_client_socket():
    try:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        config = get_config()
        client_socket.connect((config['host'], config['port']))
    except socket.gaierror as error:
        print(error)
    else:
        return client_socket


def connect_with_client(server_socket):
    try:
        conn, addr = server_socket.accept()
    except socket.gaierror as error:
        print("Address-related error connecting with the client! %s", error)
    else:
        print("connection established!")
        return conn, addr