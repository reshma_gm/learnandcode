import unittest
import socket
import server_side


class TestServerSide(unittest.TestCase):

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('localhost', 9783))

    def test_obtained_obj_json(self):
        expected = {"resh": "reshma", "resh124": "reshma"}
        result = server_side.data_from_json_file("test_data_load.json")
        self.assertEqual(result, expected)

    def test_update(self):
        src_data = { "rahul125": "Rahul Kumar" }
        new_data = { "key": "resh", "value": "Reshma" }
        expected_updated_data = { "rahul125": "Rahul Kumar", "resh": "Reshma" }
        result = server_side.update(new_data, src_data)
        self.assertEqual(result, expected_updated_data)

if __name__ == '__main__':
    unittest.main()