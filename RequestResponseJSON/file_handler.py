import pickle


def write_serialized_data(input_list):
    with open("backup.pickle", 'wb') as handler:
        pickle.dump(input_list, handler, protocol=pickle.HIGHEST_PROTOCOL)


def read_deserialized_data():
    try:
        with open("backup.pickle", 'rb') as handler:
            data = pickle.load(handler)
            return data
    except EOFError:
        data = dict()
        return data
