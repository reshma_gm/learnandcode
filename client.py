import socket

HOST = '127.0.0.1'
PORT = 9938

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
    client_socket.connect((HOST, PORT))
    while True:
        message_sent = input("Client: ")
        client_socket.send(bytes(message_sent, "utf-8"))
        if message_sent == "exit":
            break
        received_message = (client_socket.recv(1024)).decode("utf-8")
        print("Server: ",received_message)